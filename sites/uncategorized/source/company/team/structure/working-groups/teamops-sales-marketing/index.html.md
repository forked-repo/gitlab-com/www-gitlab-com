---
layout: markdown_page
title: "TeamOps Sales and Marketing"
description: "The TeamOps Sales and Marketing Working Group aims to manage the design and kickoff of relevant strategies."
canonical_path: "/company/team/structure/working-groups/teamops-sales-marketing/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value                                                                                                                                             |
|:----------------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| Date Created    | 2023-03-06                                                                                                                                        |
| Target End Date | 2023-04-31                                                                                                                                        |
| Overview        | [Issue](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/workplace/teamops-fy24-q1/-/issues/3)   |
| Slack channel   | [#wg_teamops](https://gitlab.slack.com/archives/C050C7WFE1X)   |
| Google Doc      | [Working Group Agenda](https://drive.google.com/drive/search?q=TeamOps%20Marketing%20Working%20Group%20Agenda%20FY24-Q1) (only accessible from within the company) |

## Goal

The TeamOps Sales and Marketing Working Group aims to manage the design and kickoff of relevant strategies.

### Overview

[TeamOps](/handbook/teamops/direction/) needs to generate revenue, and requires a cross-functional working group to discuss, and design one more relevant plans.

Please see the [overview issue](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/workplace/teamops-fy24-q1/-/issues/3) for more information.

### Exit Criteria

1. An approved 3-year growth plan for TeamOps Sales and Marketing.
1. Assigned counterparts for continued collaboration and implementation.
1. Identified training and enablement needs.

## Roles and Responsibilities

| Working Group Role      | Username        | Person                                                                   | Title                                                           |
| :---------------------- | :-------------- | ------------------------------------------------------------------------ | :-------------------------------------------------------------- |
| Executive Stakeholder   | @streas | Stella Treas | Chief of Staff to CEO |
| Facilitator             | @lfarrer | Laurel Farrer | Principal Strategy and Operations, Workplace |
| Member                  | @cynthia | Cynthia "Arty" Ng | Staff Strategy and Operations, Intern | 
| Member                  | @lboughner | Lisa Boughner | VP, Corporate Communications | 
| Member                  | @dsteer | Dave Steer | VP, Product Marketing | 
| Member                  | @swalters1 | Stephen Walters | Principal Solutions Architect | 
| Member                  | @esalvadorp | Emilio Salvador | VP, Dev Relations and Community | 
| Member                  | @nshah19 | Niyati Shah | Senior Director, Education Services | 
