title: Fujitsu Cloud Technologies
file_name: fujitsu
canonical_path: /customers/fujitsu/
cover_image: /images/blogimages/fjct_cover.jpg
cover_title: |
  Fujitsu Cloud Technologies improves deployment velocity and cross-functional workflows with GitLab
cover_description: |
  Fujitsu Cloud Technologies adopted GitLab as it delivered one integrated DevOps platform resulting in superior efficiency. 
twitter_image: /images/blogimages/fjct_cover.jpg
twitter_text: Learn how @Fujitsu_Global improved deployment velocity with GitLab.
customer_logo: /images/case_study_logos/FujitsuCTL_Logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Tokyo, Japan
customer_solution: GitLab Premium
customer_employees: |
  296
customer_overview: |
  Fujitsu Cloud Technologies uses GitLab for SCM, CI, and shared know-how.
customer_challenge: |
  Fujitsu Cloud Technologies teams were using different tools for each project and lacked a cohesive, collaborative DevOps platform for an efficient workflow. 
key_benefits: >-

  
    All-in-one DevOps environment  

  
    Superior support 

  
    The ability to manage the authorization in accordance with Japanese business practices

  
    Access to quick audits

  
    Improved workflow efficiency
customer_stats:
  - stat: 511
    label: users
  - stat: 526   
    label: groups
  - stat: 5,201
    label: projects
customer_study_content:
  - title: the customer
    subtitle: Leader in global cloud technology
    content: >-
    
  
       [Fujitsu Cloud Technologies](https://fjct.fujitsu.com/) is a service provider offering IaaS, PaaS, and SaaS to over 8,000 customers. Since 2010, the company has been providing software as a service through the Internet via its cloud service “NIFCLOUD”; a domestically produced public cloud service that utilizes cutting-edge technology. They contribute to a vision of a sustainable society by providing reliable cloud services that everyone can use. They aim to become the "No. 1 enterprise cloud" of Japanese quality.
     
  - title: the challenge
    subtitle:  Moving from VCS to Git 
    content: >-
    
  
        Before GitLab, each project had a separate Git and SVN management tool. It is common in Japanese business practice to bring in development and operations engineers from outside the company on a project-by-project basis. However, it is difficult to properly manage the authorization to work with internal engineers to achieve DevOps. “I wanted to create a way to share the knowledge and expertise of our internal developers and operators across projects,” said Yuichi Saotome, Principle Engineer, Cloud Infra Division, Fujitsu Cloud Technologies. 
    
  
        Due to the inability to properly manage authorizations, various tools were introduced on a project-by-project basis, causing knowledge and expertise to be siloed. They were indiscriminately deploying different tools for different teams and distributing the necessary elements of service development among the tools. Some of the tools used on a project-by-project basis included: 

          * GitBucket
          * BitBucket
          * Redmine
          * Jira
          * Jenkins
          * drone CI
          * Circle CI

    
  
        Around 2014, the momentum to unify VCS to Git grew. There were many different VCSs, but Git was able to meet a majority of their needs. The biggest priority for Saotome and his team was the ability to manage approvals in line with Japanese business practices. Secondly, it must be an all-in-one (complete) DevOps environment. “We knew that using a combination of various tools would be very wasteful, so we looked for a tool that integrated the elements we needed at the time: issue management, progress management, code management, CI, and CD,” Saotome said.
  - title: the solution
    subtitle:  Open DevOps platform for all
    content: >-
        Initially, GitLab was adopted by a small team, however, its use gradually expanded. By 2016, the entire company was using a unified GitLab environment. The platform’s ability to be used cross-functionally created a shared know-how amongst team members throughout the company.
    
  
        “All of our employees (including non-engineers) and external engineers are using it. They say, ‘GitLab has allowed the concept of project management to permeate not only the development team, but also the operations, design, and sales teams, making it [easier to share knowledge and expertise](/solutions/devops-platform/),’” according to Saotome.
    
  
        GitLab’s “excellent” authorization management allows internal engineers to freely implement DevOps on any project they want. External engineers can also implement DevOps on a per-project basis with authorization. The coexistence of internal and external authorization made it easier to share know-how and expertise without fear of information leakage or internal or external barriers. Also, the addition of the Diff function for images and the WebIDE made it easier for non-engineers to use the software and expanded the range of its use.

  - title: the results
    subtitle: Unified integrations, shared capabilities
    content: >-
    
  
        After adopting GitLab, some initial successes included the ability to migrate Git repositories under project management tools that were previously scattered. On top of that, the GitLab service operation flow was praised by Fujitsu’s audit firm for being an "excellent workflow that takes [risk management into account](/solutions/dev-sec-ops/).”
    
  
        Efficiency and quality have improved significantly with GitLab SCM. The development cycle previously took up to six months for the same team. Now, cycles can be released in as little as a few days. Team members also depend on GitLab’s monthly releases. “We're excited about the new GitLab features that come out every month,” said Saotome.


        Detecting bugs earlier in the life cycle has increased output capabilities. “Quality testing goes smoothly right before release so that release dates can be met and marketing can be executed as planned,” Saotome shared. Validation is done in conjunction with GitLab CI using an in-house tool called vCell that recreates a small, virtualized infrastructure.


        [GitLab CI](/features/continuous-integration/) has enabled teams to achieve fast deployments of small development units, such as blue-green deployments. The teams have gone from deploying once every six months (at the longest), which took about a day, to now deploying once every few weeks (at the shortest), which takes about five minutes.
  
  
        Slack, Jenkins, Prometheus, and Redmine are all plugged into GitLab. The integration has unified the procedures for issue management, progress management, code management, CI, and CD. All of these processes were previously different for each team, but GitLab’s DevOps platform has made it easier for teams to “flex their personnel and makes it easier for new members to join the team immediately,” Saotome added. 


    
  
        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/features/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: We believe it is the right tool for achieving DevOps in Japanese business practices.
    attribution: Yuichi Saotome
    attribution_title: Principal Engineer, Cloud Infra Division, Fujitsu Cloud Technologies




